#include <omp.h>
#include <stdio.h>

int main()
{
    int p;
    p =omp_get_num_procs();
	omp_set_num_threads(p);
	#pragma omp parallel
	{
		printf("procesador disponible\n");
	}

	return 0;
}

